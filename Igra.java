import java.util.Scanner;
public class Igra{
public static void main(String args[]) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Welcome to Ninja War!"); 
    System.out.println("Here are the instructions:");
    Ninja.instructions();
    Karta k = new Karta(10, 16);
    System.out.println("Ime = ");
    String ime = scan.next();
    System.out.println("Orujie = ");
    String orujie = scan.next();
    System.out.println("Napadeniq = ");
    int napadeniq = scan.nextInt();
    Ninja Makashi = new Ninja(ime, orujie, napadeniq, 4, 14);
    k.ninja = Makashi;
    GameFrame f = new GameFrame(k);
	f.karta = k;
	f.setSize(f.getMaximumSize());
	f.setVisible(true);

    
    while(true) {
    k.print();
    char posoka = scan.next().charAt(0);
    switch(posoka) {
    case 'w' : Makashi.posy--;
    if(Makashi.posy < 0) {
    	Makashi.posy = k.sizey - 1;
    }
    break;
    case 's' : Makashi.posy++; 
    if(Makashi.posy > k.sizey - 1) {
    Makashi.posy = 0;
    }
    break;
    case 'a' : Makashi.posx--;
    if(Makashi.posx < 0) {
    Makashi.posx = k.sizex - 1;
    }
    break;
    case 'd' : Makashi.posx++;
    if(Makashi.posx > k.sizex - 1) {
    Makashi.posx = 0;
    }
    break;
    case 'q' : return;
    }
 
 }
}
}
