import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
GamePanelMap panel;
	
	GameMouseListener(GamePanelMap p) {
		panel = p;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		panel.karta.ninja.posx = e.getX() / 50;
		panel.karta.ninja.posy = e.getY() / 50;
		panel.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
