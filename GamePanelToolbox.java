import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;

public class GamePanelToolbox extends Panel {
	Label label;
	TextField textField;
	TextArea textArea;
	Button button;
	
	GamePanelToolbox() {
		label = new Label();
		label.setBounds(0, 0, 200, 50);
		label.setText("Welcome to Ninja War!");
		label.setBackground(Color.BLUE);
		
		this.add(label);
		
		textField = new TextField();
		textField.setBounds(0, 100, 200, 50);
		this.add(textField);
		
		GameButtonListener gbl = new GameButtonListener(this);
		
		button = new Button();
		button.setBounds(0, 160, 200, 50);
		button.setLabel("Exit");
		button.addActionListener(gbl);
		this.add(button);
	}

		
}

