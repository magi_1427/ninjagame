import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class GameKeyListener implements KeyListener {
GamePanelMap panel;
	
	GameKeyListener(GamePanelMap p) {
		panel = p;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		panel.karta.ninja.move(c);
		panel.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		char c;
		switch(e.getKeyCode()) {
			case 38: c = 'w'; break;
			case 40: c = 's'; break;
			case 37: c = 'a'; break;
			case 39: c = 'd'; break;
			default: c = ' ';
		}
		panel.karta.ninja.move(c);
		panel.repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
